<?php

require('frog.php');
require('ape.php');

$sheep = new Animal("Shaun");

echo "Name: $sheep->nama <br>";
echo "Legs : $sheep->legs <br>";
echo "Cold Blooded : $sheep->cold_blooded <br><br>";

$kodok = new frog("buduk");
echo "Name : $kodok->nama <br>";
echo "Legs : $kodok->legs <br>";
echo "Cold Blooded : $kodok->cold_blooded <br>";
echo "Jump : ";
echo $kodok->jump();
echo "<br><br>";

$sungokong = new ape("kera sakti");
echo "Name : $sungokong->nama <br>";
echo "Legs : $sungokong->legs <br>";
echo "Cold Blooded : $sungokong->cold_blooded <br>";
echo "Yell : ";
echo $sungokong->yell();

?>